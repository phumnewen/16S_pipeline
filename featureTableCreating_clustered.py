#!/usr/bin/python3

# ~ from sys import argc
import sys


def asvFrequences (ASVID, featureTable):
	for asv in featureTable[1:]:
		currentID = int(asv[0][4:-1])
		if currentID and currentID == ASVID:
			return asv[1:]

def otuRepSeq (OTUID, fastaData):
	for otu in fastaData:
		currentID = int(otu[0].split("_")[0])
		if currentID and currentID == OTUID:
			return otu[1]

def readTableFile (featureTableFileName):
	featureTable = []
	with open (notClusterTableFileName, 'r') as featureTableFile:
		for line in featureTableFile.readlines():
			asvList = line[:-1].split(',')
			featureTable.append(asvList)
			featureTableFile.close()
	return featureTable

def readFastaFile (fastaFileName):
	seqs = []
	with open (fastaFileName, 'r') as fastaFile:
		while 1:
			line = fastaFile.readline()
			if not line: break
			if line[0] == ">":
				seqId = line[1:]
				seq = fastaFile.readline()
				seqRec = [seqId, seq[:-1]]
				seqs.append(seqRec)
		fastaFile.close()
	return seqs

def countFeaturesFreq (featureTable):
	featuresCount = 0
	for asv in featureTable[1:]:
		for freqT in asv[1:]:
			freq = int (freqT)
			if freq:
				featuresCount = featuresCount + freq
	return featuresCount

argN = len(sys.argv)
if argN<5:
	print ("Usage: featureTableCreating_clustered.py <not_clustered_table_filename> <clusters_filename>, <repseqs_filename> <output_clustered_table_filename>")
	sys.exit()

# Input files
notClusterTableFileName = sys.argv[1]
clustersFileName = sys.argv[2]
repseqsFileName = sys.argv[3]

# Output files
clusteredTableFileName = sys.argv[4]


print ("Reading input feature table...")
inputTable = readTableFile(notClusterTableFileName)
header = inputTable[0]

print ("Input sequence number: ", len(inputTable[1:]))
print ("Input Total Frequency: ", countFeaturesFreq(inputTable))

print("Reading repseqs file...")
repseqs = readFastaFile(repseqsFileName)

clusteredTable = []

print ("Counting clusters frequencies...")

with open (clustersFileName, 'r') as clustersFile:
	lines = clustersFile.readlines()
	lineCount = len(lines)
	p = 0
	for i in range(lineCount):
		line=lines[i]
		clustID = int(line.split('\t')[0].split('_')[0])
		asvInClustID = int(line.split('\t')[1].split('_')[0])
		
		if clustID and asvInClustID:
			
			freqs = asvFrequences(asvInClustID, inputTable)
			
			if asvInClustID == clustID:
				newClust = []
				clustRepSeq = otuRepSeq(clustID, repseqs)
				newClust.append(clustRepSeq)
				newClust.extend(freqs)
				clusteredTable.append(newClust)
			else:
				for i in range(len(freqs)):
					clusteredTable[-1][i+1] = int(clusteredTable[-1][i+1]) + int(freqs[i])		
		ln = int(100*i/lineCount)
		if ln > p:
			p = ln
			print(ln,"%", end="\r")
	
clustersFile.close()
	
clusteredTable.insert(0, header)

print ("Cluster number: ", len(clusteredTable[1:]))
print ("Clustered Total Frequency: ", countFeaturesFreq(clusteredTable))

clusteredTableFile = open(clusteredTableFileName, 'w')
for otu in clusteredTable:
	clusteredTableFile.write("%s\n" % otu)
clusteredTableFile.close()

