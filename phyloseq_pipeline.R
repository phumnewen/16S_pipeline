source(PHYEXT, echo=TRUE)

source(paste0(dirname(rstudioapi::getSourceEditorContext()$path), "/16S_librares_&_functions.r"), echo = TRUE)


## ---------------------------------------------------------------------------
## LOAD DATA
## ---------------------------------------------------------------------------

load(ASVRDATA)
sample.data <- readSampleData(METADATAPATH)

ps <- phyloseq(otu_table(seqtab, taxa_are_rows = TRUE), sample_data(sample.data), tax_table(taxa_slv))
# Creating Phyloseq containing phylogeny tree
# ps <- phyloseq(otu_table(seqtab, taxa_are_rows = TRUE), sample_data(sample.data), tax_table(taxa_slv), phy_tree(tree_fromfitGTR))


## ---------------------------------------------------------------------------
## PREPROCESSING
## ---------------------------------------------------------------------------

yearsAnalizing <- c("2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018")
# mergedDistance <- c("20", "6", "25", "17", "11", "13", "15", "22")
mergedDistance <- c("11", "13", "15", "17", "20", "22", "25", "6")
mergedYears <- c("2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016")
mergedNTCType <- c("MskLab", "PCRNTC", "PitNTC", "ProgressLab", "seqNTC", "ShovelNTC")

# ---------------------------------------------------------------------------
# Firstly clean the dataset
# ---------------------------------------------------------------------------

psNoNTC_hard <- setMyLevels(psNoNTC_hard)

ps <- setMyLevels(ps)

ps <- subset_taxa(ps, Kingdom=="Bacteria")
ps <- prune_samples(sample_sums(ps)>=1, ps)
ps <- prune_taxa(taxa_sums(ps)>=1, ps)

pointsAllTogether <- subset_taxa(ps, Kingdom=="Bacteria")
pointsAllTogether <- filter_taxa(pointsAllTogether, function(x) sum(x) > 3, TRUE)
pointsAllTogether <- subset_samples(pointsAllTogether, YearOfLayer %in% yearsAnalizing)
sample_data(pointsAllTogether)$YearOfLayer_r = factor(sample_data(pointsAllTogether)$YearOfLayer, levels=c("2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009"))
pointsAllTogether <- prune_taxa(taxa_sums(pointsAllTogether)>=1, pointsAllTogether)
pointsAllTogether <- prune_samples(sample_sums(pointsAllTogether)>=1, pointsAllTogether)



# ---------------------------------------------------------------------------
# Removing negative controls using Decontam
# ---------------------------------------------------------------------------

sample_data(ps)$is.neg <- sample_data(ps)$SampleType == "NTC"
contamdf.prev <- isContaminant(ps, method="prevalence", neg="is.neg", threshold = 0.999)

ps.sample = prune_samples(sample_data(ps)$SampleType == "snow", ps)
ps.noncontam <- prune_taxa(!contamdf.prev$contaminant, ps.sample)
ps.noncontam <- subset_samples(ps.noncontam, YearOfLayer %in% yearsAnalizing)
ps.noncontam <- prune_samples(sample_sums(ps.noncontam)>=1, ps.noncontam)
ps.noncontam <- prune_taxa(taxa_sums(ps.noncontam)>=1, ps.noncontam)
sample_data(ps.noncontam)$YearOfLayer_r = factor(sample_data(ps.noncontam)$YearOfLayer, levels=c("2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009"))

sum(taxa_sums(ps.noncontam))
dim(otu_table(ps.noncontam))
pointsMergedClass <- tax_glom(ps.noncontam, taxrank = "Class", bad_empty = c(""))
dim(otu_table(pointsMergedClass))
pointsMergedGenus <- tax_glom(ps.noncontam, taxrank = "Genus", bad_empty = c(""))
dim(otu_table(pointsMergedGenus))

# ---------------------------------------------------------------------------
# Removing negative controls by OUTs subtraction ("hard" approach)
# ---------------------------------------------------------------------------

psSample <- subset_samples(pointsAllTogether, SampleType == "snow")
psSample <- subset_samples(psSample, YearOfLayer %in% yearsAnalizing)
psSample <- prune_samples(sample_sums(psSample)>=1, psSample)
psSample <- prune_taxa(taxa_sums(psSample)>=1, psSample)

psNTC <- subset_samples(pointsAllTogether, SampleType == "NTC")
psNTC <- prune_samples(sample_sums(psNTC)>=1, psNTC)
psNTC <- prune_taxa(taxa_sums(psNTC)>=1, psNTC)
sample_data(psNTC)$NTCType_r = factor(sample_data(psNTC)$NTCType, levels=c("PitNTC", "ShovelNTC", "ProgressLab", "MskLab", "PCRNTC", "seqNTC"))


pointsAllTogetherNoNTC_hard <- prune_negatives(physeq = pointsAllTogether, samps = psSample, negs = psNTC)
# We will take only following years beacuse the rest are in a pair of points only
pointsAllTogetherNoNTC_hard <- subset_samples(pointsAllTogetherNoNTC_hard, YearOfLayer %in% yearsAnalizing)
pointsAllTogetherNoNTC_hard <- prune_samples(sample_sums(pointsAllTogetherNoNTC_hard)>=1, pointsAllTogetherNoNTC_hard)
pointsAllTogetherNoNTC_hard <- prune_taxa(taxa_sums(pointsAllTogetherNoNTC_hard)>=1, pointsAllTogetherNoNTC_hard)
sample_data(pointsAllTogetherNoNTC_hard)$YearOfLayer_r = factor(sample_data(pointsAllTogetherNoNTC_hard)$YearOfLayer, levels=c("2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009"))







