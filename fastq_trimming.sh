#!/bin/bash

MODE="none"
if [[ -z $1 ]]; then
 echo "No argument has been provided :("
 echo "Please provide trimming mode as an argument:"
 echo "  SE: single-end"
 echo "  PE: paired-end"
 exit 1
elif [[ $1 == 'SE' || $1 == 'PE' ]]; then
 MODE=$1
else 
 echo "Invalid argument has been provided :("
 echo "Please provide trimming mode as an argument:"
 echo "  SE: single-end"
 echo "  PE: paired-end"
fi

echo $RAWPAH
cd $RAWPAH

## Initial QC
mkdir raw_fastqc_analysis/
fastqc -t 4 -o $RAWPAH/raw_fastqc_analysis/ $RAWPAH/*

## Trimming
mkdir $TRIMPATH/

if [[$1 == 'PE' ]]; then

echo "Single-end mode set"

	## PAIRED-END MODE

	for i in `ls | sed -r "s/(.+)_R[1,2]_001.fastq.gz/\1/" | uniq | sort -d` ; do
		echo $i
		java -jar $TRIMMOMATICPATH \
		PE \
		-threads 20 \
		-phred33 \
		$RAWPAH/${i}_R1_001.fastq.gz \
		$RAWPAH/${i}_R2_001.fastq.gz \
		$TRIMPATH/${i}_R1_001.fastq.gz \
		$TRIMPATH/${i}_unpaired_R1_001.fastq.gz \
		$TRIMPATH/${i}_R2_001.fastq.gz \
		$TRIMPATH/${i}_unpaired_R2_001.fastq.gz \
		ILLUMINACLIP:$ADAPTPATH:3:30:10 SLIDINGWINDOW:1:3 MINLEN:150 HEADCROP:19 CROP:250;
	done

	## Removing unpaired reads
	rm $TRIMPATH/*unpaired*
	
elif [[$1 == 'SE' ]]; then
	
	## SINGLE-END MODE
	
	echo "Single-end mode set"
	
	for i in `ls $RAWPAH/ | sed -r "s/(.+)_001.fastq.gz/\1/" | uniq | sort -d` ; do
		echo $i
		java -jar $TRIMMOMATICPATH \
		SE \
		-threads 20 \
		-phred33 \
		$RAWPAH/${i}_001.fastq.gz \
		$TRIMPATH/${i}_001.fastq.gz \
		ILLUMINACLIP:$ADAPTPATH:3:30:10 SLIDINGWINDOW:1:3 MINLEN:50 HEADCROP:19 CROP:250; 
	done

fi


## QC after the trimming
mkdir $TRIMPATH/trimmed_fastqc_analysis/
fastqc -t 4 -o $TRIMPATH/trimmed_fastqc_analysis/ $TRIMPATH/*

