## Installation


Requires no installation

---


## Usage

Before using set paths that correct for your system. <br>
If you need to perform Phyloseq analysis and don't need to use 
DADA2, and Trimmomatic, you have to set metadata
file path only.

---


## Dependencies

Trimmomatic <br>
R >= 3.5 <br>
R packages:
- DADA2
- ggplot2
- phyloseq
- magrittr
- dplyr
- vegan
- grid
- ape
- Biostrings
- plyr
- tidyr
- parallel
- decontam
- knitr

